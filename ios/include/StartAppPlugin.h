#import "PluginManager.h"
#import <StartApp/StartApp.h>

@interface StartAppPlugin : GCPlugin

@property (nonatomic, retain) TeaLeafViewController *tealeafViewController_;
@property (nonatomic, retain) STABannerView *banner_;
@property (nonatomic, retain) STAStartAppAd *startAppAd;

@end
