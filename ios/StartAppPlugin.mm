#import "StartAppPlugin.h"

@implementation StartAppPlugin
// The plugin must call super dealloc.
- (void) dealloc {
	self.banner_ = nil;
    [self.startAppAd release];
	[super dealloc];
}

// The plugin must call super init.
- (id) init {
	self = [super init];
	if (!self) {
		return nil;
	}
	self.banner_ = nil;
    self.startAppAd = nil;
	return self;
}

- (void) initializeWithManifest:(NSDictionary *)manifest appDelegate:(TeaLeafAppDelegate *)appDelegate {
	@try {
		NSLog(@"{startapp} Initialized with manifest");
		self.tealeafViewController_ = appDelegate.tealeafViewController;
		NSDictionary *ios = [manifest valueForKey:@"ios"];
		NSString *startAppDevID = [NSString stringWithFormat:@"%@",[ios valueForKey:@"startAppUserID"]];
        NSString *startAppBannerID = [NSString stringWithFormat:@"%@",[ios valueForKey:@"startAppAdID"]];
        STAStartAppSDK* sdk = [STAStartAppSDK sharedInstance];
        sdk.appID = startAppBannerID;
        sdk.devID = startAppDevID;
//        [sdk showSplashAd];
        self.startAppAd = [[STAStartAppAd alloc] init];
        [self.startAppAd loadAd];
        NSLog(@"{startApp} Initializing from manifest with: %@ %@", startAppDevID, startAppBannerID);
	}
	@catch (NSException *exception) {
		NSLog(@"{startapp} Failure init: %@", exception);
	}
}

- (void) showBanner:(NSDictionary *)jsonObject {
	@try {
        if (self.banner_ == nil) {
            NSString *pos = [jsonObject valueForKey:@"position"];
            //banner
            if([pos isEqualToString:@"top"]){
                self.banner_ = [[STABannerView alloc] initWithSize:STA_AutoAdSize autoOrigin:STAAdOrigin_Top     
                                                withView:self.tealeafViewController_.view withDelegate:nil];
            }else{
                self.banner_ = [[STABannerView alloc] initWithSize:STA_AutoAdSize autoOrigin:STAAdOrigin_Bottom     
                                                withView:self.tealeafViewController_.view withDelegate:nil];
            }
            [self.tealeafViewController_.view addSubview:self.banner_];
            
        }
	}
	@catch (NSException *exception) {
		NSLog(@"{startapp} Failure during banner: %@", exception);
	}
}

- (void) showInterstitial:(NSDictionary *)jsonObject {
    @try {
        if([self.startAppAd isReady]){
//            NSLog(@"{startapp} ad ready");
            [self.startAppAd showAd];
            [self.startAppAd loadAd];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"{startapp} Failure during banner: %@", exception);
    }
}



@end

