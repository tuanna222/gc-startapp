var hasNativeEvents = NATIVE && NATIVE.plugins && NATIVE.plugins.sendRequest;

var startapp = Class(function () {
	this.init = function () {
	}
	// /**
	// * pos: top, bottom
	// */
	// this.showBanner = function (pos) {
	// 	// logger.log("{startapp} JS Showing banner");
	// 	if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
	// 		// logger.log("sendEvent StartAppPlugin...");
	// 		NATIVE.plugins.sendEvent("StartAppPlugin", "showBanner",
	// 			JSON.stringify({position: pos}));
	// 	}
	// };

	this.showInterstitial = function(){
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			// logger.log("sendEvent StartAppPlugin...");
			NATIVE.plugins.sendEvent("StartAppPlugin", "showInterstitial", JSON.stringify({}));
		}
	}

	this.initBanner = function (position) {
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			NATIVE.plugins.sendEvent("StartAppPlugin", "initBanner",
				JSON.stringify({position: position}));
		}
	};

	this.showBanner = function () {
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			NATIVE.plugins.sendEvent("StartAppPlugin", "showBanner",
				JSON.stringify({}));
		}
	};

	this.hideBanner = function () {
		if (NATIVE && NATIVE.plugins && NATIVE.plugins.sendEvent) {
			NATIVE.plugins.sendEvent("StartAppPlugin", "hideBanner",
				JSON.stringify({}));
		}
	};

});

exports = new startapp();
