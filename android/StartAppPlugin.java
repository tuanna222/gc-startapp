package com.tealeaf.plugin.plugins;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import com.tealeaf.EventQueue;
import com.tealeaf.TeaLeaf;
import com.tealeaf.logger;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import java.util.HashMap;

import com.tealeaf.plugin.IPlugin;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.FrameLayout;
import android.view.Gravity;
import android.view.View;


import com.tealeaf.EventQueue;
import com.tealeaf.event.*;

import com.startapp.android.publish.StartAppSDK;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.banner.Banner;

public class StartAppPlugin implements IPlugin {

	private Banner adBannerView;
	private StartAppAd startAppAd;

	public StartAppPlugin() {
	}

	public void onCreateApplication(Context applicationContext) {
	}

	public void onCreate(Activity activity, Bundle savedInstanceState) {
        PackageManager manager = activity.getPackageManager();
        String startAppDevID = "";
        String startAppBannerID = "";
        try {
            Bundle meta = manager.getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA).metaData;
            if (meta != null) {
            	startAppDevID = String.valueOf(meta.get("startAppUserID"));
                startAppBannerID = String.valueOf(meta.get("startAppAdID"));
                StartAppSDK.init(activity, startAppDevID, startAppBannerID);
                // StartAppAd.showSplash(activity, savedInstanceState);
                adBannerView = new Banner(activity);
                startAppAd = new StartAppAd(activity);
            }
        } catch (Exception e) {
            android.util.Log.d("{startApp} EXCEPTION", "" + e.getMessage());
            e.printStackTrace();
        }
        logger.log("{startApp} Initializing from manifest with startAppDevID=", startAppDevID);
        logger.log("{startApp} Initializing from manifest with startAppBannerID=", startAppBannerID);
	}

	public void initBanner(final String json) {
		logger.log("{startapp} initBanner");
		if(adBannerView != null){
			TeaLeaf.get().runOnUiThread(new Runnable()
			{
			    public void run()
			    {
			    	try {
			    		JSONObject obj = new JSONObject(json);
						final String pos = obj.getString("position");
						FrameLayout group = TeaLeaf.get().getGroup();
						if(pos.equals("top")){
							final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(
							    				FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
										group.addView(adBannerView, adViewLayoutParams);
						}else{
							final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(
							    				FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
										group.addView(adBannerView, adViewLayoutParams);
						}
						adBannerView.setVisibility(View.GONE);
						adBannerView.bringToFront();
					} catch(Exception e) {
						logger.log("{startapp} Exception while Initializing ads:", e.getMessage());
					}
			    }
			});
		}
	}

	public void showBanner(final String json) {
		if(adBannerView != null){
			TeaLeaf.get().runOnUiThread(new Runnable()
			{
			    public void run()
			    {
					logger.log("{startapp} loadAd...");
					adBannerView.setVisibility(View.VISIBLE);
				}
			});
		}
	}

	public void hideBanner(final String json) {
		if(adBannerView != null){
			TeaLeaf.get().runOnUiThread(new Runnable()
			{
			    public void run()
			    {
					logger.log("{startapp} hideBanner");
					adBannerView.setVisibility(View.GONE);
				}
			});
		}
	}

   //  public void showBanner(final String json) {
	  //   if(adBannerView != null){
	  //   	new Thread()
			// {
			//     public void run()
			//     {
			//         TeaLeaf.get().runOnUiThread(new Runnable()
			//         {
			//             public void run()
			//             {
			//             	try{
			// 	            	final JSONObject obj = new JSONObject(json);
			// 					String pos = obj.getString("position");
			// 					FrameLayout group = TeaLeaf.get().getGroup();
			// 					if(pos.equals("top")){
			// 			    		final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(
			// 			    				FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.TOP | Gravity.CENTER_HORIZONTAL);
			// 						group.addView(adBannerView, adViewLayoutParams);
			// 					    adBannerView.bringToFront();
			// 					}else{
			// 						final FrameLayout.LayoutParams adViewLayoutParams = new FrameLayout.LayoutParams(
			// 			    				FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
			// 						group.addView(adBannerView, adViewLayoutParams);
			// 					    adBannerView.bringToFront();
			// 					}
			// 				} catch (Exception e) {
			// 					logger.log("{startapp} Exception while processing event:", e.getMessage());
			// 				}
			//             }
			//         });
			//     }
			// }.start();
	  //   }
   //  }

    public void showInterstitial(final String json) {
	    if(startAppAd != null){
	    	new Thread()
			{
			    public void run()
			    {
			        TeaLeaf.get().runOnUiThread(new Runnable()
			        {
			            public void run()
			            {
			            	try{
				            	startAppAd.showAd(); // show the ad
								startAppAd.loadAd(); // load the next ad
							} catch (Exception e) {
								logger.log("{startapp} Exception while processing event:", e.getMessage());
							}
			            }
			        });
			    }
			}.start();
	    }
    }

	public void onResume() {
		startAppAd.onResume();
	}

	public void onStart() {
	}

	public void onPause() {
		startAppAd.onPause();
	}

	public void onStop() {
	}

	public void onDestroy() {
	}

	public void onNewIntent(Intent intent) {
	}

	public void setInstallReferrer(String referrer) {
	}

	public void onActivityResult(Integer request, Integer result, Intent data) {
	}

	public boolean consumeOnBackPressed() {
		return true;
	}

	public void onBackPressed() {
	}
}

